# Port Java de Rogue 1980 en Español
Traducción al español del port a Java de Rogue 1980. Si quieres ayudar, solo contáctame.
Basada en el código fuente de "TADodger": https://github.com/TADodger/JavaRogue

![Rogue 1980 carátula](https://darknromhacking.com/RogueEsp/img/Rogue_cover.jpg)

## Instalación
Muy simple, carga los fuentes en IntelliJ IDEA o Eclipse. Prueba ejecutando el archivo Rogue.java como Aplicación Java.

### Los archivos de salvada y score
Los archivos de **salvada** quedan en

`C:\Users\miuser\.rogue\rogue.ser`

Los archivos de **puntajes** quedan en

`C:\Users\miuser\.rogue\rogue.scr`

El juego al iniciar, de forma automática los toma de ahí, si existen claro.

Si cierras la aplicación y no guardas con S (s mayúscula), no se guarda nada.

## Autor
Hernaldo González (hernaldog@gmail.com): traductor principal.
Si quieres ayudar, sólo escríbeme.

## Licencia
Libre, copia lo que quieras.

## Estatus del proyecto: **Finalizado**
- Versión 1.1 04-10-2021: Cambio de "Des" por "Arm" en el menú inferior ya que en el manual habla de Armadura y no Defensa.
- Versión 1.0 21-09-2021: Me terminé el juego para validar últimos téxtos se leyeran bien.
- Versión 0.9 17-09-2021: Traducción del 100% de los textos. Varias mejoras en la interpretación.
- Versión 0.7 14-09-2021: traducción de casi todos los textos. Ahora se trabaja en que tengan sentido y se lean bien en español.
- Versión 0.5 11-09-2021: traducción de la mitad de los textos.
- Versión 0.1 06-09-2021: traducción básica de algunos textos.

## Descarga
- [v1.1 - 04-10-2021 Rogue Español JAR](https://darknromhacking.com/RogueEsp/Rogue-esp-1.1.jar)
- [v1.0 - 21-09-2021 Rogue Español JAR](https://darknromhacking.com/RogueEsp/Rogue-esp-1.0.jar)

Solo necesitas tener Java 1.8 o Java 11 instalado en tu equipo. Ver video con el paso a paso: https://youtu.be/nxx1uwxBCs4

## Manual
El manual lo iré traduciendo de a poco al español: https://darknromhacking.com/RogueEsp/rogue1980-manual-esp.html

## Imágenes
Una imagen dice "Dark-N" ya que son de mi sitio segundario de traducciones de juegos y romhacking: https://darknromhacking.com/

![Rogue Java Esp Demo 1](https://darknromhacking.com/RogueEsp/img/rogue-java-espanol-demo.png)
![Rogue Java Esp Demo 3](https://darknromhacking.com/RogueEsp/img/rogue-java-espanol-demo3.png)
![Rogue Java Esp Finalizado](https://darknromhacking.com/RogueEsp/img/rogue-java-finalizado.png)

