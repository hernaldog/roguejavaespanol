package rogue;

import java.io.Serializable;

/**
 *
 */
public class Trap extends Item implements Serializable {
    private static final long serialVersionUID = -5822054677475787076L;

    /** */
    public int kind;
    /** */
    public static final int TRAP_DOOR = 0;
    /** */
    public static final int BEAR_TRAP = 1;
    /** */
    public static final int TELE_TRAP = 2;
    /** */
    public static final int DART_TRAP = 3;
    /** */
    public static final int SLEEPING_GAS_TRAP = 4;
    /** */
    public static final int RUST_TRAP = 5;
    /** */
    public static final int TRAPS = 6;

    private static String msg[] = { "$ cayó en una trampa", "$están atrapados en una trampa para osos", "teletransporta", "un pequeño dardo acaba de golpear $ en el hombro", "una extraña niebla blanca envuelve $ y $se queda dormido",
            "un chorro de agua golpea $ en la cabeza" };
    /**
     * 
     */
    public static String name[] = { "puerta trampa", "trampa de oso", "trampa de teletransporte", "trampa de dardos venenosos", "trampa de gas para dormir", "trampa en escalera", };

    /**
     * @param level
     * @param r
     * @param c
     * @param kind
     */
    public Trap(Level level, int r, int c, int kind) {
        super(level, r, c);
        this.kind = kind;
        placeAt(r, c, TRAP);
    }

    /**
     * @param p
     * @return The message for this trap
     */
    public String trapMessage(Persona p) {
        String src = msg[kind];
        String dst = "";
        int i = 0;
        int j;
        try {
            while ((j = src.indexOf('$', i)) >= 0) {
                dst += src.substring(i, j);
                i = j + 1;
                if (src.charAt(i) != ' ') {
                    dst += "@>" + p.name() + '+';
                    if (src.charAt(i) == 'a') {
                        dst += "are+is<";
                        i += 3;
                    } else {
                        j = src.indexOf(' ', j);
                        dst += src.substring(i, j) + '+' + src.substring(i, j) + '<';
                        i = j;
                    }
                } else
                    dst += "@<" + p.name() + ">";
            }
        } catch (Exception e) {
            System.out.println("trapMessage(" + p.name() + ")\n\t" + src + '\n' + dst);
        }
        dst += src.substring(i);
        return dst;
    }
}
