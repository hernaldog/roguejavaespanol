package rogue;

import java.io.Serializable;

/**
 *
 */
public class Potion extends Toy implements Serializable {
    private static final long serialVersionUID = 2698734724537852689L;

    private static final String STRANGE_FEELING = "extraña sensación";

    /**
     * @param level
     */
    public Potion(Level level) {
        super(level, Id.POTION);
    }

    /**
     * 
     */
    public void quaffby() {
        switch (kind) {
            case Id.INCREASE_STRENGTH:
                owner.tell("¡Te sientes mas fuerte ahora, que grandes músculos!");
                owner.strCurrent++;
                if (owner.strCurrent > owner.strMax) {
                    owner.strMax = owner.strCurrent;
                }
                break;
            case Id.RESTORE_STRENGTH:
                owner.strCurrent = owner.strMax;
                owner.tell("Esto sabe muy bien, te sientes caliente por todas partes");
                break;
            case Id.HEALING:
                owner.tell("Comienzas a sentirte mejor");
                potionHeal(false);
                break;
            case Id.EXTRA_HEALING:
                owner.tell("Comienzas a sentirte mucho mejor");
                potionHeal(true);
                break;
            case Id.POISON:
                if ((owner instanceof Man) && ((Man) owner).sustainStrength) {
                    break;
                }
                owner.strCurrent -= level.rogue.rand.get(1, 3);
                if (owner.strCurrent < 1) {
                    owner.strCurrent = 1;
                }
                owner.tell("Te comienzas a sentir muy enfermo");
                if (owner.halluc > 0) {
                    owner.unhallucinate();
                }
                break;
            case Id.RAISE_LEVEL:
                if (owner instanceof Man) {
                    ((Man) owner).expPoints = Man.LEVEL_POINTS[((Man) owner).exp - 1];
                    owner.tell("De repente te sientes mucho más hábil");
                    ((Man) owner).add_exp(1, true);
                }
                break;
            case Id.BLINDNESS:
                owner.goBlind();
                break;
            case Id.HALLUCINATION:
                owner.tell("Oh wow, todo parece tan cósmico");
                owner.halluc += level.rogue.rand.get(500, 800);
                break;
            case Id.DETECT_MONSTER:
                if ((owner instanceof Man) && !level.showMonsters((Man) owner)) {
                    owner.tell(STRANGE_FEELING);
                }
                break;
            case Id.DETECT_TOYS:
                if (level.levelToys.size() > 0 && (owner instanceof Man)) {
                    if (owner.blind != 0)
                        level.showToys((Man) owner);
                } else
                    owner.tell(STRANGE_FEELING);
                break;
            case Id.CONFUSION:
                owner.tell(owner.halluc > 0 ? "Que sensación más alucinante" : "Te sientes confuso");
                owner.cnfs(level.rogue.rand.get(12, 22));
                break;
            case Id.LEVITATION:
                owner.describe(owner.who("comienzas") + "a flotar en el aire", false);
                owner.levitate += level.rogue.rand.get(15, 30);
                owner.beingHeld = false;
                owner.bearTrap = 0;
                break;
            case Id.HASTE_SELF:
                owner.tell("Sientes que te mueves mucho más rápido");
                owner.hasteSelf += level.rogue.rand.get(11, 21);
                owner.hasteSelf += 1 - (owner.hasteSelf & 1);
                break;
            case Id.SEE_INVISIBLE:
                if (owner instanceof Man) {
                    owner.tell("Hmm, esta poción sabe a jugo de " + ((Man) owner).option.fruit);
                    if (owner.blind > 0) {
                        owner.unblind();
                    }
                    ((Man) owner).seeInvisible = true;
                    ((Man) owner).view.markall(); // relight
                }
                break;
        }
        owner.printStat();
        Id.identifyUncalled(kind);
        vanish();
    }

    private void potionHeal(boolean extra) {
        double ratio;
        int add;

        owner.hpCurrent += owner.exp;

        ratio = ((double) owner.hpCurrent) / owner.hpMax;

        if (ratio >= 1.00) {
            owner.hpMax += (extra ? 2 : 1);
            owner.extra_hp += (extra ? 2 : 1);
            owner.hpCurrent = owner.hpMax;
        } else if (ratio >= 0.90) {
            owner.hpMax += (extra ? 1 : 0);
            owner.extra_hp += (extra ? 1 : 0);
            owner.hpCurrent = owner.hpMax;
        } else {
            if (ratio < 0.33) {
                ratio = 0.33;
            }
            if (extra) {
                ratio += ratio;
            }
            add = (int) (ratio * ((double) owner.hpMax - owner.hpCurrent));
            owner.hpCurrent += add;
            if (owner.hpCurrent > owner.hpMax) {
                owner.hpCurrent = owner.hpMax;
            }
        }
        owner.healPotional(extra);
    }
}
