package rogue;

import java.io.Serializable;

/**
 *
 */
public class Scroll extends Toy implements Serializable {
    private static final long serialVersionUID = 3154221148022434821L;

    /**
     * @param level
     */
    public Scroll(Level level) {
        super(level, Id.SCROLL);
    }

    /**
     * 
     */
    public void readby() {
        switch (kind) {
            case Id.SCARE_MONSTER:
                owner.tell("Escuchas una risa maníaca a la distancia");
                break;
            case Id.HOLD_MONSTER:
                holdMonster();
                break;
            case Id.ENCH_WEAPON:
                if (owner.weapon != null) {
                    if (0 != (owner.weapon.kind & Id.WEAPON)) {
                        String plural = owner.weapon.quantity <= 1 ? "s " : " ";
                        owner.tell("Tu " + owner.weapon.name() + "brilla" + plural + owner.getEnchColor());
                        if (level.rogue.rand.coin()) {
                            owner.weapon.hitEnchant++;
                        } else {
                            owner.weapon.dEnchant++;
                        }
                    }
                    owner.weapon.isCursed = false;
                } else {
                    owner.tell("Tienes hormigeo en las manos");
                }
                break;
            case Id.ENCH_ARMOR:
                if (owner.armor != null) {
                    owner.tell("Por un momento, tu armadura brilla " + owner.getEnchColor());
                    owner.armor.dEnchant++;
                    owner.armor.isCursed = false;
                    owner.printStat();
                } else {
                    owner.tell("Tu piel queda expuesta");
                }
                break;
            case Id.IDENTIFY:
                if (owner instanceof Man) {
                    owner.tell("Esto es un pergamino de identificación");
                    identified = true;
                    Id.idScrolls[kind & 255].idStatus = Id.IDENTIFIED;
                    Toy t = ((Man) owner).find(Id.ALL_TOYS, "¿Qué quieres identificar?", "");
                    if (t != null) {
                        t.identified = true;
                        Id.identify(t.kind);
                        owner.tell(t.getDesc());
                    }
                }
                break;
            case Id.TELEPORT:
                owner.tele();
                break;
            case Id.SLEEP:
                owner.describe(owner.who("Caes") + "dormido", false);
                owner.takeANap();
                break;
            case Id.PROTECT_ARMOR:
                if (owner.armor != null) {
                    owner.tell("Tu armadura está cubierta por un escudo de oro reluciente");
                    owner.armor.isProtected = true;
                    owner.armor.isCursed = false;
                } else {
                    owner.tell("Tu acné parece haber desaparecido");
                }
                break;
            case Id.REMOVE_CURSE:
                if (owner instanceof Man) {
                    owner.tell(owner.halluc == 0 ? "Sientes como si alguien te estuviera cuidando" : "Te sientes en contacto con el universo");
                    ((Man) owner).pack.uncurseAll();
                }
                break;
            case Id.CREATE_MONSTER:
                if (!createMonster(owner)) {
                    owner.tell("Escuchas un suave grito de angustia a la distancia");
                }
                break;
            case Id.AGGRAVATE_MONSTER:
                owner.tell("Escuchas un agudo zumbido");
                aggravate(owner);
                break;
            case Id.MAGIC_MAPPING:
                owner.tell("Este pergamino parece tener un mapa");
                if (owner instanceof Man) {
                    level.drawMagicMap((Man) owner);
                }
                break;
            case Id.CON_MON:
                owner.conMon = true;
                owner.tell("Por un momento, tus manos brillan de color " + owner.getEnchColor());
                break;
        }
        Id.identifyUncalled(kind);
        vanish();
    }

    private boolean createMonster(Persona man) {
        int perm[] = level.rogue.rand.permute(9);
        for (int i = 0; i < 9; i++) {
            int c = Id.X_TABLE[perm[i]] + man.col;
            int r = Id.Y_TABLE[perm[i]] + man.row;
            if ((r == man.row && c == man.col) || r < MIN_ROW || r > level.numRow - 2 || c < 0 || c > level.numCol - 1) {
                continue;
            }
            if (0 == (level.map[r][c] & MONSTER) && 0 != (level.map[r][c] & (FLOOR | TUNNEL | STAIRS | DOOR))) {
                Monster monster = level.getRandomMonster();
                monster.putMonsterAt(r, c);
                level.rogue.vset(r, c);
                if (0 != (monster.mFlags & (Monster.WANDERS | Monster.WAKENS))) {
                    monster.wakeUp();
                }
                return true;
            }
        }

        return false;
    }

    private void aggravate(Persona man) {
        for (Monster monster : level.levelMonsters) {
            monster.wakeUp();
            monster.mFlags &= ~Monster.IMITATES;
            level.rogue.vset(monster.row, monster.col);
        }
    }

    private void holdMonster() {
        int mcount = 0;
        for (int i = -2; i <= 2; i++) {
            for (int j = -2; j <= 2; j++) {
                int r = owner.row + i;
                int c = owner.col + j;
                if (r < MIN_ROW || r > level.numRow - 2 || c < 0 || c > level.numCol - 1) {
                    continue;
                }
                if (0 != (level.map[r][c] & MONSTER)) {
                    Monster monster = level.levelMonsters.itemAt(r, c);
                    if (monster != null && monster != owner) {
                        monster.mFlags |= Monster.ASLEEP;
                        monster.mFlags &= ~Monster.WAKENS;
                        mcount++;
                    }
                }
            }
        }
        String s = "Los monstruos alrededor de " + owner.who() + " se quedan congelados";
        if (mcount == 0) {
            s = owner.who("siente") + "una extraña sensación de pérdida";
        } else if (mcount == 1) {
            s = "El monstruo se congela";
        }
        owner.describe(s, false);
    }
}
