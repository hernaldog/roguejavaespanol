package rogue;

/**
 *
 */
public class Identifychar {
    private static char[] characterCmd = new char[48];
    private static String[] characterDesc = new String[48];

    static {
        characterCmd[0] = '?'; characterDesc[0] = "?       muestra la ayuda";
        characterCmd[1] = 'r'; characterDesc[1] = "r       leer pergamino";
        characterCmd[2] = '/'; characterDesc[2] = "/       identificar objeto";
        characterCmd[3] = 'e'; characterDesc[3] = "e       comer";
        characterCmd[4] = 'h'; characterDesc[4] = "h       izquierda ";
        characterCmd[5] = 'w'; characterDesc[5] = "w       empuñar un arma";
        characterCmd[6] = 'j'; characterDesc[6] = "j       abajo";
        characterCmd[7] = 'W'; characterDesc[7] = "W       ponerse una armadura";
        characterCmd[8] = 'k'; characterDesc[8] = "k       arriba";
        characterCmd[9] = 'T'; characterDesc[9] = "T       sacarse la armadura";
        characterCmd[10] = 'l'; characterDesc[10] = "l       derecha";
        characterCmd[11] = 'P'; characterDesc[11] = "P       ponerse un anillo";
        characterCmd[12] = 'y'; characterDesc[12] = "y       arriba-izquierda";
        characterCmd[13] = 'R'; characterDesc[13] = "R       sacarse un anillo";
        characterCmd[14] = 'u'; characterDesc[14] = "u       arriba-derecha";
        characterCmd[15] = 'd'; characterDesc[15] = "d       tirar objeto";
        characterCmd[16] = 'b'; characterDesc[16] = "b       abajo-izquierda";
        characterCmd[17] = 'c'; characterDesc[17] = "c       renombrar objeto";
        characterCmd[18] = 'n'; characterDesc[18] = "n       abajo-derecha";
        characterCmd[19] = '\0'; characterDesc[19] = "<SHIFT><dir>: correr en esa dirección";
        characterCmd[20] = ')'; characterDesc[20] = ")       muestra arma actual";
        characterCmd[21] = '\0'; characterDesc[21] = "<CTRL><dir>: correr hasta la adyacente";
        characterCmd[22] = ']'; characterDesc[22] = "]       muestra armadura actual";
        characterCmd[23] = 'f'; characterDesc[23] = "f<dir>  pelear hasta la muerte o casi morir";
        characterCmd[24] = '='; characterDesc[24] = "=       muestra anillos equipados";
        characterCmd[25] = 't'; characterDesc[25] = "t<dir>  lanza algo";
        characterCmd[26] = '\001'; characterDesc[26] = "^A      muestra aumento promedio de HP";
        characterCmd[27] = 'm'; characterDesc[27] = "m<dir>  moverse sin coger nada";
        characterCmd[28] = 'z'; characterDesc[28] = "z<dir>  agitar una varita en una dirección";
        characterCmd[29] = 'o'; characterDesc[29] = "o       examinar/setear opciones";
        characterCmd[30] = '^'; characterDesc[30] = "^<dir>  identificar tipo de trampa";
        characterCmd[31] = '\022'; characterDesc[31] = "^R      redibujar pantalla";
        // characterCmd[32]='&'; characterDesc[32]= "& save screen into 'rogue.screen'";
        characterCmd[33] = 's'; characterDesc[33] = "s       buscar trampas/puerta secreta";
        characterCmd[34] = '\020'; characterDesc[34] = "^P      repetir último mensaje";
        characterCmd[35] = '>'; characterDesc[35] = ">       bajar las escaleras";
        characterCmd[36] = '\033'; characterDesc[36] = "^[      cancelar comando";
        characterCmd[37] = '<'; characterDesc[37] = "<       subir las escaleras";
        // characterCmd[38]='S'; characterDesc[38]= "S save game";
        characterCmd[39] = '.'; characterDesc[39] = ".       esperar un turno";
        characterCmd[40] = 'Q'; characterDesc[40] = "Q       salir";
        characterCmd[41] = ','; characterDesc[41] = ",       coger algo";
        // characterCmd[42]='!'; characterDesc[42]= "! shell escape";
        characterCmd[43] = 'i'; characterDesc[43] = "i       inventario";
        characterCmd[44] = 'F'; characterDesc[44] = "F<dir>  pelear hasta que alguno de los dos muera";
        // characterCmd[45]='I'; characterDesc[45]= "I inventory single item";
        characterCmd[46] = 'v'; characterDesc[46] = "v       mostrar versión del juego";
        characterCmd[47] = 'q'; characterDesc[47] = "q       beber poción";
        characterDesc[19] = "";
        characterDesc[21] = "";
        characterDesc[26] = "";
        characterDesc[38] = "";
        characterDesc[42] = "";
        characterDesc[45] = "";
    }

    /**
     * Populate the Message based on the command character
     * 
     * @param ch
     * @param message
     */
    public static void cmdsList(char ch, Message message) {
        if (ch == '*' || ch == '?') {
            message.rightlist(characterDesc, false);
        } else {
            String[] desc = new String[1];
            desc[0] = "No existe comando: " + ch;
            for (int k = 0; k < characterCmd.length; k++) {
                if (ch == characterCmd[k]) {
                    desc[0] = characterDesc[k];
                    break;
                }
            }
            message.rightlist(desc, false);
        }
    }
}
