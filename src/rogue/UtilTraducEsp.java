package rogue;

//import com.sun.org.apache.xpath.internal.operations.Neg;
//import java.util.Locale;

public class UtilTraducEsp {
	
	/**
     * Transformaciones de Modal como inventario, wwar, etc, para un mejor el espa�ol
     * @param mensaje Mensaje original
     * @return mensaje transformado
     */
    public static String transfModal(String mensaje) {
		String regexNumero = "(\\-.*[0-9].*)|(\\+.*[0-9].*)";	  //cualquier -1,+1, +0 +1,-2 etc

		// Primera letra en mayúscula
		String primeraLetra = mensaje.substring(0, 1);
		primeraLetra = primeraLetra.toUpperCase();
		String resto = mensaje.substring(1);
		mensaje = primeraLetra + resto;



		if (mensaje.contains(" +") || mensaje.contains(" -") ||  mensaje.matches(regexNumero)) {
			String nuevoDesc = "";


			String[] partesDesc = mensaje.split(" ");
			int largoDesc = partesDesc.length;

			if (largoDesc == 3) {
				if (partesDesc[0].matches(regexNumero)) {
					nuevoDesc = partesDesc[1] + " "  + partesDesc[2] + " " + partesDesc[0];
					mensaje = nuevoDesc;
				}
			}
			else
			if (largoDesc == 4) {
				if (partesDesc[1].matches(regexNumero)) {
					nuevoDesc = partesDesc[0] + " " + partesDesc[2] + " " + partesDesc[3] + " " + partesDesc[1];
					mensaje = nuevoDesc;
				}
			}
			else
			if (largoDesc == 5) {
				if (partesDesc[0].matches(regexNumero)) {
					nuevoDesc = partesDesc[1] + " " + partesDesc[2] + " " + partesDesc[3] + " " + partesDesc[4] + " " + partesDesc[0];
					mensaje = nuevoDesc;
				}
				else
				if (partesDesc[2].matches(regexNumero)) {
					nuevoDesc = partesDesc[0] + " "  + partesDesc[1] + " " + partesDesc[3] + " " + partesDesc[4] + " " + partesDesc[2];
					mensaje = nuevoDesc;
				}
			}
			else
			if (largoDesc == 6) {
				if (partesDesc[1].matches(regexNumero)) {
					nuevoDesc = partesDesc[0] + " " + partesDesc[2] + " " + partesDesc[3] + " " + partesDesc[4] + " " + partesDesc[5] + " " + partesDesc[1];
					mensaje = nuevoDesc;
				}
			}
			else
			if (largoDesc == 7) {
				if (partesDesc[2].matches(regexNumero)) {
					nuevoDesc = partesDesc[0] + " " + partesDesc[1] + " " + partesDesc[3] + " " + partesDesc[4] + " " + partesDesc[5] + " " + partesDesc[6] + " " + partesDesc[2];
					mensaje = nuevoDesc;
				}
			}
		}

		if (mensaje.contains("Empuñando")) {
			mensaje = mensaje.replace("Empuñando Una", "Empuñando");
			mensaje = mensaje.replace("Empuñando Un", "Empuñando");
		}

		if (mensaje.toLowerCase().contains("maza") && (mensaje.contains("+") || mensaje.contains("-"))) {
			mensaje = mensaje.replace("maza", "");
			mensaje = mensaje.replace(" +", " maza +");
			mensaje = mensaje.replace(" -", " maza -");
		}
		
		if (mensaje.toLowerCase().contains("flecha")) {
			mensaje = mensaje.replace("flechas", "");
			mensaje = mensaje.replace(" +", " flechas +");
		}
		
		if (mensaje.toLowerCase().contains("arco corto")) {		
			mensaje = mensaje.replace("arco corto", "");
			mensaje = mensaje.replace(" +", " arco corto +");
		}

		if (mensaje.contains("Un espada")) {
			mensaje = mensaje.replace("Un espada", "espada");
		}

		if (mensaje.length()>20) {
			if (mensaje.substring(0,5).contains("malla")) {
				mensaje = mensaje.replace("malla", "Una malla");
			}
			else
			if (mensaje.substring(0,8).contains("armadura")) {
				mensaje = mensaje.replace("armadura", "Una armadura");
			}
		}
		if (mensaje.contains("izquierda llevas")) {
			mensaje = mensaje.replace("izquierda llevas Un", "izquierda llevas");
		}
		if (mensaje.contains("derecha llevas")) {
			mensaje = mensaje.replace("derecha llevas Un", "derecha llevas");
		}
		if (mensaje.contains("Una poción llamado")) {
			mensaje = mensaje.replace("Una poción llamado", "Una poción llamada");
		}

		return mensaje;
    }
    
    
    /**
     * Transformaciones para funcion Tell() para un mejor español
     * @param mensaje Mensaje original
     * @return mensaje transformado
     */
    public static String transfTell(String mensaje) {
    	
		// Primera letra en mayúscula
		String primeraLetra = mensaje.substring(0, 1);
		primeraLetra = primeraLetra.toUpperCase();
		String resto = mensaje.substring(1);
		mensaje = primeraLetra + resto;
    	        
    	mensaje = mensaje.replaceFirst(" :", ":");
		mensaje = mensaje.replaceFirst("^ *", ""); //eliminamos cualquier primer espacio del texto
		mensaje = mensaje.replace(" Un ", " un ");
		mensaje = mensaje.replace(" Una ", " una ");
		mensaje = mensaje.replace("  ", " ");

		String elLa = "El";
		if (mensaje.toLowerCase().contains("serpiente") || mensaje.toLowerCase().contains("medusa") || mensaje.toLowerCase().contains("trampa voladora")) {
			elLa = "La";
		}
		if (mensaje.contains("golpe criatura")) {
			mensaje = mensaje.replace("golpe criatura", "golpe - " + elLa);
		}
		if (mensaje.contains("golpear")) {
			mensaje = mensaje.replace("golpear criatura", "golpear - " +elLa);
		}
		if (mensaje.contains("Criatura ")) {
			mensaje = mensaje.replace("Criatura", elLa);
		}
		if (mensaje.contains("Mataste ")) {
			String alLa = "al";
			if (mensaje.toLowerCase().contains("serpiente") || mensaje.toLowerCase().contains("medusa") || mensaje.toLowerCase().contains("trampa voladora")) {
				alLa = "a la";
			}
			mensaje = mensaje.replace("Mataste criatura", "Mataste " + alLa);
		}
		if (mensaje.contains("Dejaste un ")) {
			mensaje = mensaje.replace("Dejaste un ", "Dejaste ");
		}
		if (mensaje.contains("Dejaste algo comida")) {
			mensaje = mensaje.replace("Dejaste algo comida", "Dejaste caer la comida");
		}
		if (mensaje.contains("Dejaste un pergamino")) {
			mensaje = mensaje.replace("Dejaste un pergamino", "Dejaste uno o más pergaminos");
		}
		if (mensaje.contains("Dejaste una espada")) {
			mensaje = mensaje.replace("Dejaste una espada", "Dejaste espada");
		}
		if (mensaje.contains("Te pones")) {
			mensaje = mensaje.replace("Te pones una ", "Te pones ");
		}
		if (mensaje.contains("izquierda llevas anillo")) {
			mensaje = mensaje.replace("En mano izquierda llevas", "Te pones en mano izquierda");
		}
		else
		if (mensaje.contains("derecha llevas anillo")) {
			mensaje = mensaje.replace("En mano derecha llevas", "Te pones en mano derecha");
		}
		if (mensaje.contains("Te quitas una")) {
			mensaje = mensaje.replace("Te quitas una", "Te quitas");
		}else
		if (mensaje.contains("Te quitas un")) {
			mensaje = mensaje.replace("Te quitas un", "Te quitas");
		}
		if (mensaje.contains("No se pudo coger")) {
			mensaje = mensaje.replace("No se pudo coger una", "No se pudo coger");

			if (mensaje.contains("Algo comida")) {
				mensaje = mensaje.replace("Algo comida", "comida");
			}
		}

		return mensaje;
    }

	public static String transfTellEquipado(String mensaje) {
		mensaje = mensaje.replace("izquierda llevas", "izquierda llevas equipado");
		mensaje = mensaje.replace("derecha llevas", "derecha llevas equipado");
		return mensaje;
	}

}
